/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbc_p1;

/**
 *
 * @author OSCAR
 */
public class CV_CONOCIMIENTOS_TECNICOS {
    
    String Herramienta,NivelExperiencia,Obligatorio,Certificacion,Prioridad;
    
    public CV_CONOCIMIENTOS_TECNICOS()
    {
        Herramienta = NivelExperiencia = Obligatorio = Certificacion = Prioridad =  "";        
    }
    public CV_CONOCIMIENTOS_TECNICOS(String H,String NE, String O, String C, String P)
    {
        Herramienta = H;
        NivelExperiencia = NE;
        Obligatorio = O;
        Certificacion = C;
        Prioridad = P;
                
    }
    public String getHerramienta()
    {
        return Herramienta;
    }
    public String getNivelExperiencia()
    {
        return NivelExperiencia;        
    }
    public String getObligatorio(){
        return Obligatorio;
    }
    public String getCertificacion(){
        return Certificacion;
    }
    public String getPrioridad()
    {
        return Prioridad;
    }
            
    
    
}
