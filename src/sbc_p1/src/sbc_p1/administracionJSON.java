package sbc_p1;
import java.io.*;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class administracionJSON {
	
		public static int suma=0; 
		public static float porcentaje=0;
		
		public static void grita(String a){
			System.out.println(a);
		}
		
		public static void krpeta(String name) throws IOException {
		    File dir = new File(name);
		    if (dir.exists()) {
		    	grita("ya existe la carpeta (Y)");
		        return ;//dir;
		    }
		    grita("se intentara crear la carpeta");
		    if (dir.mkdirs()) {
		    	grita("carpeta creada");
		        return ;//dir;
		    }
		    throw new IOException("la carpeta no pudo ser creada :c");
		}
		
		private static JSONObject leerJson(String s) {
			JSONParser parser = new JSONParser();
            Object otto;
			try (FileReader fr = new FileReader(s) ) {	           	            		
           		 
	            otto = parser.parse(fr);
	            return  (JSONObject) otto;                        
	            
	        } catch (IOException | ParseException e) {
	            e.printStackTrace();
	        }
			return null;
		}
		
		public static void main(String [] args){
			
		/*	try {
				krpeta("gorila");
				krpeta("propuestas");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
			JSONObject ocv = leerJson("curriculums//bergoCV.json");
			JSONObject ore = leerJson("propuestas//propuesta5.json");
			
			
			
			String mensaje="";
			
			int e1=entero(ore.get("Edadm")),e2=entero(ore.get("EdadM")),e3=entero(ocv.get("Edad"));
			mensaje +="Edad : "+edades(e1,e2,e3)+"\n";
			
			String sore=(String)ore.get("Sexo"),socv=(String)ocv.get("Sexo");			
			mensaje +="Sexo: "+sexualidad(sore,socv)+"\n";
			
			String more=(String)ore.get("Modalidad"),mocv=(String)ocv.get("Modalidad");
			mensaje +="Modalidad : "+modalidad(more,mocv)+"\n";
			
			int asore=entero(ore.get("Aspiracion_Salarial")), asocv=entero(ocv.get("Aspiracion_Salarial"));
			mensaje +="Aspiracion Salarial : "+salario(asore,asocv)+"\n";
			
			String vore=(String)ore.get("Disponibilidad_de_viaje"), vocv=(String)ocv.get("Disponibilidad_de_viaje");
			mensaje +="Disponibilidad de Viaje : "+viaje(vore,vocv)+"\n";
			
			JSONObject mapa=(JSONObject) ore.get("Localizacion");
			JSONArray z = (JSONArray) mapa.get("Zona");  
			String dir=(String) ocv.get("Direccion"),pz=(String) mapa.get("Prioridad");
			mensaje += "Localizaci�n : "+direccion(z,dir,pz)+"\n";
			
			JSONArray neore=(JSONArray) ore.get("Nivel_de_Educacion");
			String neocv= (String) ocv.get("Nivel_de_Educacion");
			mensaje += "Nivel de educacion : "+titulo(neore,neocv)+"\n";
			
			JSONArray core=(JSONArray) ore.get("Cargo"), cocv=(JSONArray) ocv.get("Cargo");
			mensaje += "Cargo tentativo :"+cargo(core,cocv)+"\n";
					
		}
				
		private static int calprioridad(String p){
			// se retorna un numero dada la prioridad
			
			if (p.equalsIgnoreCase("alto"))
				return 6;
			
			else if (p.equalsIgnoreCase("medio"))
				return 4;
			
			else if (p.equalsIgnoreCase("bajo"))
				return 2;
			
			return 0;//caso de no importa ...
		}	
		
				
		private static String cargo(JSONArray core, JSONArray cocv) {
			String r="";
			for(Object o  : core){
				JSONObject aux = (JSONObject) o;		
				String n = (String) aux.get("Nombre");
				
				porcentaje+=5;
				//cada objeto representa un cargo en el requerimiento
				//por ende cada cargo sera de 5 ptos
				for(Object q : cocv){
					JSONObject x= (JSONObject) q;
					String m = (String) x.get("Nombre");
					
					if(n.equalsIgnoreCase(m)){ 
						// el candidato ha tenido experiencia en ese cargo
						
						r+="El candidato conoce: "+m;
						int are=entero(aux.get("Anhos_de_experiencia")), 
								acv=entero(x.get("Anhos_de_experiencia"));
						
						if(acv>=are){
							// 5ptos por tener la cantidad de a�os minima en ese cargo
							r+="Con una cantidad de "+acv+" a�os";
							suma+=5;
						}
						else if(acv>0){
							//2 ptos por haber tenido aunque sea unos a�os en ese cargo 
							suma+=2;
						}
						r+="\n";
					}
				}
			}
			return r;
		}

		private static String titulo(JSONArray neore, String neocv) {
			//String [] s  = {"Bachiller","Tecnico Medio tm ","Tecnico Superior Universitario tsu",
				//	"Ingeniero ing","Magister msc.","Doctor"};
			
			//mayor para saber cual grado mayor piden de Requerimiento
			//otro como aux para ir viendo 1 por 1 cual es el titulo que encaja con el candidato
			//grado para ver cual es el mayor grado del solicitante
			
			int mayor=0,  grado=0;
			
			
			for(Object diploma:  neore){
				JSONObject aux = (JSONObject) diploma;
				
				//otro =calprioridad((String) aux.get("Prioridad"));
												
				if(calprioridad((String) aux.get("Prioridad"))>mayor) 
					mayor=calprioridad((String) aux.get("Prioridad"));
				
				if(neocv.equalsIgnoreCase((String) aux.get("Titulo"))){
					//tiene un titulo prometedor el solicitante
					grado=mayor;
					//se guarda el mayor grado en los titulos del solicitante
				}					
			}
			porcentaje+=mayor;
			suma+=grado;
			
			if(grado>0)// el solicitante cumplio el requisito 
				return "El solicitante tiene un titulo valioso de: "+neocv;
						
			return "El solicitante no cuenta con la preparacion necesaria";
		}

		private static String direccion(JSONArray z, String dir, String p) {
			
			int aux=calprioridad(p);
			//si no es prioridad la direccion no importa este modulo
			if(aux==0)
				return "la direccion no es importante";
						
			dir=dir.toLowerCase();
			
			porcentaje+=aux;
			
			for(Object  s : z){
				if(dir.contains(((String)s).toLowerCase())){
					suma+=aux;
					return "El solicitante se encuentra en el area deseada";
				}
			}
			
			return "El solicitante debe encontrarse en las cercanias";
		}
		

		private static String viaje(String vore, String vocv) {
			if(vore.equalsIgnoreCase("si")){
				porcentaje+=2;
				
				if(!vore.equalsIgnoreCase(vocv))				
					return "El solicitante debe tener la posibilidad de viajar";
				
				else
					suma+=2;
			}
			
			return vocv;
		}

		private static String salario(int asore, int asocv) {
			if(asocv<=asore){
				return "El solicitante aspira a una cantidad realista"+ (asocv < asore?", de paso pide "+(asore-asocv)+"$ menos":"");
			}
			return "El solicitante aspira a una cantidad mayor de la establecida";
		}

		private static String modalidad(String more, String mocv) {
			if(!more.equalsIgnoreCase(mocv))
				return "El solicitante debe adaptarse a la modalidad";
			return mocv;
		}

		private static String sexualidad(String sore, String socv) {
			
			if(!sore.equals("Otro") && !sore.equalsIgnoreCase(socv))
				return "El solicitante debe cumplir con un genero en especifico";
			return socv;
		}

		private static String edades(int e1, int e2, int e3) {
			//validacion del rango de edades
			String mensaje;
			if(e1!=-1 ){
				if(e2!=-1){
					if(e1<=e3 && e3<=e2){
						mensaje="La edad esta en el rango valido";
					}
					else{//no va pal baile
						mensaje="No cumple con la edad requerida";
					}
				}				
				else{//no hay edad maxima
					if(e3>=e1){
						mensaje="La edad esta en el rango valido";
					}
					else{//es menor de edad
						mensaje="No cumple con la edad requerida";
					}
				}
			}
			else{ //no hay edad minima
				if(e2>=e3){ //la edad del solicitante es permitible
					mensaje="La edad esta en el rango valido";
				}
				else{// paso su edad
					mensaje="No cumple con la edad maxima requerida";					
				}
			}
			return mensaje;			
		}

		private static int entero(Object o) {			
			try{
				return Integer.parseInt((String)o);
			}catch(Exception e){
				
			}
			return -1;
		}
}
